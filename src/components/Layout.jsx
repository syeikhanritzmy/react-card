import React from 'react';
import NavigationBar from './elements/NavigationBar';
import Card from './elements/Card';
import { Stack } from 'react-bootstrap';
import Footer from './elements/Footer';

const LoopComp = () => {
  const row = [];
  for (let i = 1; i < 9; i++) {
    row.push(<Card />);
  }
  return row;
};

export default function Layout() {
  return (
    <div>
      <Stack gap={3}>
        <NavigationBar />
        <div className="d-flex justify-content-around flex-wrap gap-3 mt-5">
          {LoopComp()}
        </div>
        <Footer />
      </Stack>
    </div>
  );
}
