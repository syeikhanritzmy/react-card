import React from 'react';

function Footer() {
  return (
    <div className="bg-dark d-flex justify-content-center text-light p-5">
      {' '}
      copyright ©️
    </div>
  );
}

export default Footer;
